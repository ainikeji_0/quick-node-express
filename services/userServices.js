const { quickMd5, getDate } = require("../utils/index");
const {
  selectOneWhere,
  selectTotal,
  selectPage,
  insert,
  update,
  updateField,
  remove,
} = require("../model/userModel");
const { getUserRoleListByUserId } = require("./userRoleServices");
const { getRoleMenuListByRoleIds } = require("./roleMenuServices");
const {
  getMenuListByIds,
  getMenuList,
} = require("./menuServices");

class UserServices {
  constructor() {}
  async getUserPermission(userId) {
    const userRoleList = await getUserRoleListByUserId(userId);
    const roleIdArr = [];
    const menuIdArr = [];
    userRoleList.forEach(async (element) => {
      const roleId = element.role_id;
      roleIdArr.push(roleId);
    });
    if (roleIdArr.length <= 0) {
      return {
        status: 0,
        msg: "授权菜单列表",
        data: [],
      };
    }
    const roleIds = roleIdArr.join(",");
    const roleMenuResult = await getRoleMenuListByRoleIds(roleIds);
    const roleMenuList = roleMenuResult.data;
    roleMenuList.forEach((element) => {
      const menuId = element.menu_id;
      const index = menuIdArr.indexOf(menuId);
      if (index == -1) {
        menuIdArr.push(menuId);
      }
    });
    if (menuIdArr.length <= 0) {
      return {
        status: 0,
        msg: "授权菜单列表",
        data: [],
      };
    }
    const menuListResult = await getMenuList();
    const menuList = menuListResult.data;

    const menuIdArrNew = [];
    menuIdArr.forEach((menuId) => {
      menuIdArrNew.push(menuId);
      const menu = menuList.find((x) => {
        return x.id === menuId;
      });
      const pid = menu.pid;
      const index = menuIdArrNew.indexOf(pid);
      if (pid !== 0 && index === -1) {
        menuIdArrNew.push(pid);

        const parentMenu = menuList.find((x) => {
          return x.id === pid;
        });
        const pid1 = parentMenu.pid;
        const index1 = menuIdArrNew.indexOf(pid1);
        if (pid1 !== 0 && index1 === -1) {
          menuIdArrNew.push(pid1);
        }
      }
    });
    const menuIds = menuIdArrNew.join(",");
    return await getMenuListByIds(menuIds);
  }
  async userLogin(loginInfo) {
    const { userName, userPassword } = loginInfo;
    const fields = ["user_name"];
    const values = [userName];
    const result = await selectOneWhere(fields, values);
    const user = result.results && result.results[0];
    const password = user && user.password;
    const passwordMd5 = quickMd5(userPassword);
    const token = quickMd5("123456");
    let jsonObj = {};
    if (password !== passwordMd5) {
      jsonObj = {
        status: 1,
        msg: "登录失败，用户名或密码错误",
        data: null,
      };
    }
    jsonObj = {
      status: 0,
      msg: "登录成功",
      data: {
        token: token,
      },
    };
    return jsonObj;
  }
  async getUserPageList(current, size, userName) {
    let pageIndex = 1;
    let pageSize = 10;
    if (current < 1) {
      current = 1;
    }
    if (size < 1) {
      size = 10;
    }
    current = parseInt(current);
    size = parseInt(size);
    pageIndex = size * (current - 1);
    pageSize = size;
    const pageListResult = await selectPage(pageIndex, pageSize, userName);
    const pageList = pageListResult.results && pageListResult.results;
    const totalResult = await selectTotal(userName);
    const total = totalResult.results && totalResult.results[0].total;
    const jsonObj = {
      status: 0,
      msg: "订单分页列表",
      data: pageList,
      page: {
        current,
        size,
        total,
      },
    };
    return jsonObj;
  }
  async getUserDetail(id) {
    const fields = ["id"];
    const values = [id];
    const result = await selectOneWhere(fields, values);
    const user = result.results && result.results[0];
    const jsonObj = {
      status: 0,
      msg: "用户详情",
      data: user,
    };
    return jsonObj;
  }
  async getUserInfo(userName) {
    const fields = ["user_name"];
    const values = [userName];
    const result = await selectOneWhere(fields, values);
    const user = result.results && result.results[0];
    const jsonObj = {
      status: 0,
      msg: "登录用户信息",
      data: user,
    };
    return jsonObj;
  }
  async addUser(user) {
    user.password = quickMd5(quickMd5("123456")); //初始密码123456，密码两次加密，代表前端一次，后端一次
    user.create_time = getDate();
    await insert(user);
    const jsonObj = {
      status: 0,
      msg: "添加用户成功",
      data: null,
    };
    return jsonObj;
  }
  async updateUser(user) {
    await update(user);
    const jsonObj = {
      status: 0,
      msg: "修改用户成功",
      data: null,
    };
    return jsonObj;
  }
  async removeUser(id) {
    await remove(id);
    const jsonObj = {
      status: 0,
      msg: "删除用户成功",
      data: null,
    };
    return jsonObj;
  }
  async batchRemoveUser(ids) {
    await remove(ids);
    const jsonObj = {
      status: 0,
      msg: "删除用户成功",
      data: null,
    };
    return jsonObj;
  }
  async updateUserPassword(user) {
    let password = quickMd5(user.newPassword);
    const fields = ["password"];
    const values = [password];
    await updateField(user.id, fields, values);
    const jsonObj = {
      status: 0,
      msg: "修改用户密码成功",
      data: null,
    };
    return jsonObj;
  }
  async resetUserPassword(id) {
    let password = quickMd5(quickMd5("123456")); //初始密码123456，密码两次加密，代表前端一次，后端一次
    const fields = ["password"];
    const values = [password];
    await updateField(id, fields, values);
    const jsonObj = {
      status: 0,
      msg: "重置用户密码成功",
      data: null,
    };
    return jsonObj;
  }
  async enabledUser(id) {
    const fields = ["enabled"];
    const values = [0];
    await updateField(id, fields, values);
    const jsonObj = {
      status: 0,
      msg: "启用用户成功",
      data: null,
    };
    return jsonObj;
  }
  async disableUser(id) {
    if (!id) {
      return null;
    }
    const fields = ["enabled"];
    const values = [1];
    await updateField(id, fields, values);
    const jsonObj = {
      status: 0,
      msg: "禁用用户成功",
      data: null,
    };
    return jsonObj;
  }
}
module.exports = new UserServices();
