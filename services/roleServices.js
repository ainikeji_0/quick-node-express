const {
  select,
  insert,
  update,
  remove
 } = require("../model/roleModel");
 const {
  getRoleMenuListByRoleIds,
  bindMenuForRole
} =require('./roleMenuServices')
const {
  getMenuListByIds,
} =require('./menuServices')

class RoleServices {
  constructor() {}
  async getPermissionMenuListByRoleId(roleId){
    const menuIdArr=[]
    const roleMenuReuslt=await getRoleMenuListByRoleIds(roleId)   
    const roleMenuList=roleMenuReuslt.data
    roleMenuList.forEach(element => {
      const menuId=element.menu_id
      const index=menuIdArr.indexOf(menuId)
      if(index==-1){
        menuIdArr.push(menuId) 
      }
    });
    if(menuIdArr.length<=0){
      return {
        status: 0,
        msg: "授权菜单列表",
        data:[]
      };
     }
    const menuIds= menuIdArr.join(',')
    return await getMenuListByIds(menuIds);
  }
  async getRoleList() {    
    const roleListResult = await select();
    const roleList = roleListResult.results && roleListResult.results;
    const jsonObj = {
      status: 0,
      msg: "角色列表",
      data:roleList
    };
    return jsonObj;
  }
  async addRole(role) {
    await insert(role);
    const jsonObj = {
      status: 0,
      msg: "添加角色成功",
      data: null,
    };
    return jsonObj;
  }
  async updateRole(role) {
    await update(role);
    const jsonObj = {
      status: 0,
      msg: "修改角色成功",
      data: null,
    };
    return jsonObj;
  }
  async deleteRole(id) {
    await remove(id);
    const jsonObj = {
      status: 0,
      msg: "删除角色成功",
      data: null,
    };
    return jsonObj;
  }
  
  async assignPermission(roleId,menuIds) {    
    const jsonObj =await bindMenuForRole(roleId,menuIds); 
    return jsonObj;
   }
}
module.exports = new RoleServices();
