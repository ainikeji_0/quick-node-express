const {
  selectWhere,
  insert,
  update,
  remove
 } = require("../model/dictionaryModel");
 
class DictionaryServices {
  constructor() {}
  async getDictionaryList(dicTypeId) {    
    const dictionaryListResult = await selectWhere(dicTypeId);
    const dictionaryList = dictionaryListResult.results && dictionaryListResult.results;
    const jsonObj = {
      status: 0,
      msg: "字典列表",
      data:dictionaryList
    };
    return jsonObj;
  }
  async addDictionary(dictionary) {
    await insert(dictionary);
    const jsonObj = {
      status: 0,
      msg: "添加字典成功",
      data: null,
    };
    return jsonObj;
  }
  async updateDictionary(dictionary) {
    await update(dictionary);
    const jsonObj = {
      status: 0,
      msg: "修改字典成功",
      data: null,
    };
    return jsonObj;
  }
  async deleteDictionary(id) {
    await remove(id);
    const jsonObj = {
      status: 0,
      msg: "删除字典成功",
      data: null,
    };
    return jsonObj;
  }
}
module.exports = new DictionaryServices();
