const {
  userLogin
  } = require("../services/userServices");
  class LoginController {
    login(tenant,userName,userPassword){
        return userLogin(tenant,userName,userPassword)
    }
  }
  module.exports=new LoginController()