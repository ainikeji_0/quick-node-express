const {
  getUserPermission,
  getUserPageList,
  getUserDetail,
  getUserInfo,
  addUser,
  updateUser,
  removeUser,
  batchRemoveUser,
  updateUserPassword,
  resetUserPassword,
  enabledUser,
  disableUser,
} = require("../services/userServices");

class UserController {
  constructor() {}
  getPermission(body){
    const {id:userId}=body
    return getUserPermission(userId)
  }
  getPageList(query) {
    const { userName, current, size } = query;
    return getUserPageList(current, size, userName);
  }
  getDetail(query) {
    const { id } = query;
    return getUserDetail(id);
  }
  getInfo(query) {
    const { userName } = query;
    return getUserInfo(userName);
  }
  add(body) {
    const user = body;
    return addUser(user);
  }
  update(body) {
    const user = body;
    return updateUser(user);
  }
  remove(body) {
    const { id } = body;
    return removeUser(id);
  }
  batchRemove(body) {
    const { ids } = body;
    return batchRemoveUser(ids);
  }
  updatePassword(body) {
    const { id, newPassword } = body;
    return updateUserPassword(id, newPassword);
  }
  resetPassword(body) {
    const { id } = body;
    return resetUserPassword(id);
  }
  enabled(body) {
    const { id } = body;
    return enabledUser(id);
  }
  disable(body) {
    const { id } = body;
    return disableUser(id);
  }
}
module.exports = new UserController();
