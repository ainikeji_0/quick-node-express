class DB {
  constructor() {
    this.mysql = require("mysql");
    this.config = require("./db.config");
  }
  query(sql, params, callBack) {
    //1.创建mysql连接对象
    const connection = this.mysql.createConnection(this.config);
    //2.打开
    connection.connect((err) => {
      if (err) {
        console.log("数据库连接失败");
        throw err;
      }
      console.log("数据库连接成功");
    });
    //3.执行sql查询
    connection.query(sql, params, (err, results, fields) => {
      if (err) {
        console.log("数据库操作失败");
        throw err;
      }
      console.log("数据库操作成功");
      callBack && callBack({ results, fields });
    });
    //4.关闭连接
    connection.end((err) => {
      if (err) {
        console.log("数据库关闭失败");
        throw err;
      }
      console.log("数据库关闭成功");
    });
  }

  queryAsync(sql, params) {
    const self = this;
    return new Promise((resolve, reject) => {
      //1.创建mysql连接对象
      const connection = self.mysql.createConnection(this.config);
      //2.打开
      connection.connect((err) => {
        if (err) {
          console.log("数据库连接失败");
          reject(err);
          return;
        }
        console.log("数据库连接成功");
      });
      //3.执行sql查询
      connection.query(sql, params, (err, results, fields) => {
        if (err) {
          console.log("数据库操作失败");
          reject(err);
          return;
        }
        console.log("数据库操作成功");
        resolve({
          results,
          fields,
        });
      });
      //4.关闭连接
      connection.end((err) => {
        if (err) {
          console.log("数据库关闭失败");
          reject(err);
          return;
        }
        console.log("数据库关闭成功");
      });
    });
  }
}

module.exports = new DB();
