'use strict';
const db = require("../config/dbPool");

class MenuModel {
  select() {
    const sql = "select * from sys_menus";
    const sqlArr = [];
    return db.queryAsync(sql, sqlArr);
  }
  selectWhere(ids) {
    const sql = `select * from sys_menus where id in(${ids})`;
    const sqlArr = [];
    return db.queryAsync(sql, sqlArr);
  }
  selectOne(id) {
    const sql = `select * from sys_menus where id=?`;
    const sqlArr = [id];
    console.log('11111111111111');
    return db.queryAsync(sql, sqlArr);
  }
  insert(menu) {
    const sql =
      "insert into sys_menus(menu_id, menu_name, path, menu_type, icon, sort, pid, link, enabled, status) values(?,?,?,?,?,?,?,?,?,?)";
    const sqlArr = [
      menu.menuId,
      menu.menuName,
      menu.path,
      menu.menuType,
      menu.icon,
      menu.sort,
      menu.pid,
      menu.link,
      menu.enabled,
      menu.status
    ];
    return db.queryAsync(sql, sqlArr);
  }
  update(menu) {
    const sql =
      "update sys_menus set menu_id = ?, menu_name = ?, path = ?, menu_type = ?, icon = ?, sort = ?, pid = ?, link = ?, enabled = ?, status = ? where id= ?";
    const sqlArr = [
      menu.menuId,
      menu.menuName,
      menu.path,
      menu.menuType,
      menu.icon,
      menu.sort,
      menu.pid,
      menu.link,
      menu.enabled,
      menu.status,
      menu.id
    ];
    return db.queryAsync(sql, sqlArr);
  }
  remove(id) {
    const sql = "delete from sys_menus where id=?";
    const sqlArr = [id];
    return db.queryAsync(sql, sqlArr);
  }
}
module.exports = new MenuModel();
