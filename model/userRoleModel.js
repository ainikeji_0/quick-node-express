'use strict';
const db = require("../config/dbPool");

class UserRoleModel {
  select() {
    const sql = "select * from per_user_roles";
    const sqlArr = [];
    return db.queryAsync(sql, sqlArr);
  }
  selectWhere( field, value){
    const sql = `select * from per_user_roles where ${field}=?`;
    const sqlArr = [value];
    return db.queryAsync(sql, sqlArr);
  }
  insert(dic) {
    const sql =
      "insert into per_user_roles(role_id,role_name) values(?,?)";
    const sqlArr = [
      dic.roleId,
      dic.roleName,
    ];
    return db.queryAsync(sql, sqlArr);
  }
  update(dic) {
    const sql =
      "update per_user_roles set role_name=? where id=?";
    const sqlArr = [
      dic.roleId,
      dic.roleName,
      dic.id,
    ];
    return db.queryAsync(sql, sqlArr);
  }
  remove(id) {
    const sql = "delete from per_user_roles where id=?";
    const sqlArr = [id];
    return db.queryAsync(sql, sqlArr);
  }
}
module.exports = new UserRoleModel();
