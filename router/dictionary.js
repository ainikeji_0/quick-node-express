const express = require("express");
const router = express.Router();
const {
  getList,
  add,
  update,
  remove,
} = require("../controllers/dictionaryController");

router.get("/api/dictionary/getList", (req, res) => {
  const { query } = req;
  getList(query).then((data) => {
    res.send(data);
  });
});
router.post("/api/dictionary/add", (req, res) => {
  const { body } = req;
  add(body).then((data) => {
    res.send(data);
  });
});
router.post("/api/dictionary/update", (req, res) => {
  const { body } = req;
  update(body).then((data) => {
    res.send(data);
  });
});
router.post("/api/dictionary/delete", (req, res) => {
  const { body } = req;
  remove(body).then((data) => {
    res.send(data);
  });
});
module.exports = router;
