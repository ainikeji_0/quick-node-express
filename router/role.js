const express = require("express");
const router = express.Router();
const {
  getMenuPermission,
  getList,
  add,
  update,
  remove,
  assignMenu,
} = require("../controllers/roleController");

router.get("/api/role/getMenuPermission", (req, res) => {
  const { query } = req;
  getMenuPermission(query).then((data) => {
    res.send(data);
  });
});
router.get("/api/role/getList", (req, res) => {
  const { query } = req;
  getList(query).then((data) => {
    console.log('data',data);
    res.send(data);
  });
});
router.post("/api/role/add", (req, res) => {
  const { body } = req;
  add(body).then((data) => {
    res.send(data);
  });
});
router.post("/api/role/update", (req, res) => {
  const { body } = req;
  update(body).then((data) => {
    res.send(data);
  });
});
router.post("/api/role/delete", (req, res) => {
  const { body } = req;
  remove(body).then((data) => {
    res.send(data);
  });
});
router.post("/api/role/assignPermission", (req, res) => {
  const { body } = req;
  assignMenu(body).then((data) => {
    res.send(data);
  });
});
module.exports = router;
